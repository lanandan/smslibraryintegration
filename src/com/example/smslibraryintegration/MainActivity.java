package com.example.smslibraryintegration;
import java.io.IOException;
import java.net.MalformedURLException;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {
public static EditText msg,mobileno;
public static Button send;
public static String uid="9966518886",phoneno,message,password="iaminlove";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mobileno=(EditText)findViewById(R.id.editText1);
        msg=(EditText)findViewById(R.id.editText2);
        send=(Button)findViewById(R.id.button1);
        send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				phoneno=mobileno.getText().toString();
				message=msg.getText().toString().trim().replace(" ", "+");
				if(message.trim()!=null&&phoneno!=null)
				{
				new SendMsg(phoneno,message,uid,password).execute(); 
				}
				else
				{
					Toast.makeText(getApplicationContext(),"Empty Message/Empty Mobile No",Toast.LENGTH_LONG).show();
				}
			      		
			}
		});
    }
class SendMsg extends AsyncTask<Void, Void, String>{
	ProgressDialog pd=new ProgressDialog(MainActivity.this);
	String phoneno,message,uid,password;
	SendMsg(String phoneno, String message, String uid, String password){
	this.phoneno=phoneno;
	this.message=message;
	this.uid=uid;
	this.password=password;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		pd.setMessage("Sending message");
		pd.show();
		pd.setCancelable(false);
	    pd.setCanceledOnTouchOutside(false);
	}

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
	String url="https://site2sms.p.mashape.com/index.php?pwd="+password+"&uid=9966518886&msg="+message+"&phone="+phoneno;
	UrlParser parser=new UrlParser();
	String data;
	try {
		data = parser.SentData(url);
		return data;
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  
	return null;
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		pd.dismiss();
		try {
			JSONObject res=new JSONObject(result);
			String answer=res.getString("response");
			Toast.makeText(getApplicationContext(),"The msg is "+answer,Toast.LENGTH_LONG).show();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}

}
