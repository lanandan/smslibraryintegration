package com.example.smslibraryintegration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class UrlParser {

	public String SentData(String url) throws MalformedURLException,IOException
	{
	StringBuilder output=new StringBuilder();
	String result = null;	
	URL _url=new URL(url);
	URLConnection _connection=_url.openConnection();
	_connection.setRequestProperty("X-Mashape-Key","NQBhNP2O0tmshDxKM17KqQiUIi0mp1hemJdjsnRNcgKgo0Nfqo");
	_connection.setRequestProperty("Accept","application/json");
	BufferedReader in = new BufferedReader(new InputStreamReader(_connection.getInputStream()));
    String inputLine;
    while ((inputLine = in.readLine()) != null)
    {
    result = ""+output.append(inputLine);
    }
        in.close();
	return result;
	}
}
